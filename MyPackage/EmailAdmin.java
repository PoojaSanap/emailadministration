package MyPackage;

import java.util.Scanner;

public class EmailAdmin {
	private String FirstName;
	private String MiddleName;
	private String SurName;
	private String DepartmentName;
	private int mailboxCapacity;
	private String NewPassword;
	private String password;
	private String EmailId;
	
	public EmailAdmin(String FirstName,String MiddleName, String SurName)
	{
		this.FirstName=FirstName;
		this.MiddleName=MiddleName;
		this.SurName=SurName;
		System.out.println("Email Created: "+this.FirstName+" "+this.SurName);
		this.DepartmentName= setDepartmentName();
		System.out.println("Department:"+this.DepartmentName);
		this.password=randomPassword(5);
		System.out.println("Password:"+this.password);
		this.EmailId=generateEmailId();
		System.out.println("Email Id:"+this.EmailId);
		this.mailboxCapacity=setMailboxCapacity(400);
		
		System.out.println("Mail Box Capacity:"+this.mailboxCapacity+"mb");
		this.NewPassword=setChangePassword();
		System.out.println("New Password :"+this.NewPassword);
		
		
	}
	private String setDepartmentName() 
	{
		
		System.out.print("Department code for: ");
		Scanner In=new Scanner(System.in);
		int depchoice=In.nextInt();
		if(depchoice==1) {return "Sales";}
		else if(depchoice==2) {return "Devlopment";}
		else if(depchoice==3) {return "Accounting";}
		else if(depchoice==4) {return "None";}
		else { return "";}
	}
	
	private String randomPassword(int length)
	{
		String passwordSet="qwertyuiopasdfghjklzxcvbnm123456789!@#$%";
		
		char[] password=new char[length];
		for(int i=0;i<length;i++)
		{
			int rand=(int)(Math.random()*passwordSet.length());
			password[i]=passwordSet.charAt(rand);
		}
		return new String(password);
	}
	
	public int setMailboxCapacity(int capacity)
	{
		return this.mailboxCapacity=capacity;
		
		
	}
	
	
	
	public void changePassword(String password)
	{
		this.password=password;
	}
	public String  generateEmailId()
	{
		EmailId=this.FirstName+'.'+this.SurName+'@'+this.DepartmentName+"company.com";
		return new String(EmailId);
	}
	
	private String setChangePassword() 
	{
		
		System.out.print("Enter New Password: ");
		Scanner In=new Scanner(System.in);
		String NewPassword=In.next();
		
		return this.NewPassword = NewPassword;
	}
	public static void main(String[] args) {
		EmailAdmin obj=new EmailAdmin("Pooja","Arunoday","Sanap");

	}

}
